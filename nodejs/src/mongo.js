//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

const axios = require("axios");

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notFlixDB?replicaSet=rs0';

let d = new Date();
let time = d.getTime();
let systemleader = 1;

//to send
var os = require("os");
var myhostname = os.hostname();
let listOfNodes = [];
var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = {"hostname" : myhostname, "status": "alive","nodeID":nodeID} ;


setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var NotFlixSchema = new Schema({
  AccountID: Number,
  UserName: String,
  TitleID: Number,
  UserAction: String,
  DateTime: String,
  PointofInteraction: String,
  TypeofInteraction: String
});

var NotFlixModel = mongoose.model('analytics', NotFlixSchema, 'analytics');

app.get('/', (req, res) => {
  NotFlixModel.find({},'UserName', (err, NotFlix) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(NotFlix))
  }) 
})

app.post('/',  (req, res) => {
  var new_NotFlix_instance = new NotFlixModel(req.body); 
  new_NotFlix_instance.save(function (err) { 
  if (err) res.send('Error'); 
    res.send(JSON.stringify(req.body)) 
  }); 
}) 

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

// pub
//use the amqplib
var amqp = require('amqplib/callback_api');
function pubsub(){
//connect to the MQ cluster
setInterval(function() {

amqp.connect('amqp://test:test@coursework_haproxy_1', function(error0, connection) {

    //if connection failed throw error
    if (error0) {
        throw error0;
    }

    //create a channel if connected and send hello world to the logs Q
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'logs';
        var msg =  JSON.stringify(toSend);

        channel.assertExchange(exchange, 'fanout', {
                durable: false
        });
        
        channel.publish(exchange, '', Buffer.from(msg));
        console.log(" [x] Sent %s", msg);
     });

           
     //in 1/2 a second force close the connection
     setTimeout(function() {
         connection.close();
     }, 500);
});
}, 4000);

//sub

amqp.connect('amqp://test:test@coursework_haproxy_1', function(error0, connection) {
  
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');
                                channel.consume(q.queue, function(msg) {
                                            if(msg.content) {
                                              let date = new Date();
                                              let t = -(time - date.getTime());
                                              let lastMsg = t / 1000;
                                                    let n = JSON.parse(msg.content.toString("utf-8")).hostname;
                                                    let id = JSON.parse(msg.content.toString("utf-8")).nodeID;
                                                            listOfNodes.some(nodes => nodes.hostname === n) ? (listOfNodes.find(e=>e.hostname === n)).lastMessage = lastMsg : listOfNodes.push({"hostname":n,"nodeID":id, "lastMessage":lastMsg, "isAlive" : "alive"});
                                                            console.log(listOfNodes);
                                                           
                                                          }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});
//code for leadership election

setInterval(function(){
let double = new Date().getTime();
let tubes = -(time - double);
let lastMessage = tubes / 1000;
   let leader = 1;
   listOfNodes.forEach(function(elements){
    let timesince = lastMessage - elements.lastMessage;
    console.log("timeSince " + timesince);
    console.log("node timeSeen " + elements.lastMessage);
      if(elements.hostname === myhostname || timesince > 10){
        
      }else{
        if(elements.nodeID > nodeID){
          leader = 0;
        }
      }
   });
   systemleader = (leader ===1) ? 1 : 0;
}, 10000);

async function createContainer(hostname){
  try{
      await axios.post(`http://host.docker.internal:2375/containers/${hostname}/start`);
  }
  catch(error)
  {
      console.log(error);
  }
}

setInterval(function() {
  if (systemleader === 1){
    console.log("**********I am the leader of this group of nodes**********");
    //leadership responsibilities
    let double = new Date().getTime();
    let tubes = -(time - double);
    let lastMessage = tubes / 1000;
    listOfNodes.forEach(function(elements){
      let timesince = lastMessage - elements.lastMessage;
      if(timesince > 10 && elements.isAlive === "alive"){
        console.log("ahhhhhhh");
        elements.isAlive = "dead";
        createContainer(elements.hostname);
      }else{
       elements.isAlive = "alive";
        }
   });
  }
}, 8000);
}
setTimeout(pubsub,20000);

